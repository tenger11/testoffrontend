import React, { Component } from 'react';
import './App.css';
import MuiThemProvider from 'material-ui/styles/MuiThemeProvider';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import NavBar from './components/navbar/NavBar';
import Balance from './components/balance/Balance';
import DateTask from './components/datetask/DateTask';
import Search from './components/search/Search';
import Note from './components/note/Note';
class App extends Component {
  
  render() {
    return (
      <Router>
        <MuiThemProvider>
          <div>
            <NavBar />
            <Route path="/" exact component={Search} />
            <Route path="/balance" exact component={Balance} />
            <Route path="/task" exact component={DateTask} />
            <Route path="/note" exact component={Note} />
          </div>
        </MuiThemProvider>
      </Router>
    );
  }
}

export default App;
