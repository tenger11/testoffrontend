import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

class DateTask extends Component {
    state = {
        searchText: '20th Sep 2041, 6th Dec 1918, 22nd May 1925, 6th Nov 1925, 19th Oct 2051, 6th Nov 2060, 18th Apr 2032, 6th Jul 2044, 1st Sep 1991'+
        '19th Jan 2029, 3rd Jan 2099, 2nd Aug 2054, 15th Jul 2047, 28th Mar 1915, 20th Nov 1946, 14th Sep 1995, 27th Oct 1977, 14th Jan 2025,'+
        '5th Nov 1944, 23rd Jun 2072',
        searchText2: '20th Oct 2052, 6th Jun 1933, 26th May 1960, 20th Sep 1958, 16th Mar 2068, 25th May 1912, 16th Dec 2018, 26th Dec 2061, '+
           '4th Nov 2030, "28th Jul 1963',
        formatDate: [["20th Sep 2041","6th Dec 1918","22nd May 1925","6th Nov 1925","19th Oct 2051","6th Nov 2060","18th Apr 2032","6th Jul 2044","1st Sep 1991",
           "19th Jan 2029","3rd Jan 2099","2nd Aug 2054","15th Jul 2047","28th Mar 1915","20th Nov 1946","14th Sep 1995","27th Oct 1977","14th Jan 2025","5th Nov 1944",
           "23rd Jun 2072"], ["20th Oct 2052", "6th Jun 1933","26th May 1960", "20th Sep 1958", "16th Mar 2068", "25th May 1912", "16th Dec 2018", "26th Dec 2061"
           ,"4th Nov 2030", "28th Jul 1963"]],
        formattedDate: '',
        formattedDate2: ''
           
    }

    getMonthNumber(date){
        let monthNames = {
            "Jan" : "01", 
            "Feb" : "02", 
            "Mar" : "03", 
            "Apr" : "04", 
            "May" : "05", 
            "Jun" : "06", 
            "Jul" : "07", 
            "Aug" : "08", 
            "Sep" : "09", 
            "Oct" : "10", 
            "Nov" : "11", 
            "Dec" : "12"
        } 

        return monthNames[date];
    }
    
    getFormattedDate(){
        let formattedText = '';
        let formattedText2 = ''
        
        this.state.formatDate[0].forEach(date => {
            date = date.replace(/(\d+)(st|nd|rd|th)/, "$1");
            const fulldate = date.split(' ');
            let getDay = fulldate[0];
            let getMonth = fulldate[1];
            let getYear = fulldate[2];

            // formattedDate.push([getYear, this.getMonthNumber(getMonth), ("0" + getDay).slice(-2)].join('-'));
            formattedText += [getYear , this.getMonthNumber(getMonth), ("0" + getDay).slice(-2)].join('-') + ",";
        }); 
        formattedText = formattedText.substring(0, formattedText.length - 1);
        this.setState({
            formattedDate: formattedText
        })

        this.state.formatDate[1].forEach(date => {
            date = date.replace(/(\d+)(st|nd|rd|th)/, "$1");
            const fulldate = date.split(' ');
            let getDay = fulldate[0];
            let getMonth = fulldate[1];
            let getYear = fulldate[2];

            // formattedDate.push([getYear, this.getMonthNumber(getMonth), ("0" + getDay).slice(-2)].join('-'));
            formattedText2 += [getYear , this.getMonthNumber(getMonth), ("0" + getDay).slice(-2)].join('-') + ",";
        }); 
        formattedText2 = formattedText2.substring(0, formattedText2.length - 1);
        this.setState({
            formattedDate2: formattedText2
        })
    }

    
    render() {
        return (
        <div className="container">
            <TextField
                name="searchText"
                value={this.state.searchText}
                floatingLabelText="Dates"
                fullWidth={true}
                multiLine={true}
                rows={2}
                rowsMax={4}
                disabled={true}
            />
            <TextField
                name="outPut"
                value={this.state.formattedDate}
                floatingLabelText="Formatted Dates"
                fullWidth={true}
                multiLine={true}
                rows={2}
                rowsMax={4}
            />
            <br />
            <TextField
                name="searchText2"
                value={this.state.searchText2}
                floatingLabelText="Dates"
                fullWidth={true}
                multiLine={true}
                rows={2}
                rowsMax={4}
                disabled={true}
            />
            <TextField
                name="outPut2"
                value={this.state.formattedDate2}
                floatingLabelText="Formatted Dates"
                fullWidth={true}
                multiLine={true}
                rows={2}
                rowsMax={4}
            />
            <br />
            <FlatButton label="Format Date" primary={true} onClick={this.getFormattedDate.bind(this)} />
        </div>
        )
    }
}

export default DateTask;
