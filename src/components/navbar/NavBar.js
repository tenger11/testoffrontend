import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import {MenuItem } from 'material-ui';
import { Link } from 'react-router-dom';
import NavRes from './NavRes';
const styles = {
    appBar: {
    flexWrap: 'wrap',
    },
    items: {
        marginTop: '8px',
        color: 'rgb(255, 255, 255)',
        fontWeight: 'bold'
    },
};
class Navbar extends Component {

    constructor(props) {
        super(props);
        this.child = React.createRef();
    }

    toggleSidenav = () => {
       this.child.current.handleToggle();
    }
    render() {
        return (
            <div>
                <AppBar title="React App" onLeftIconButtonClick={ (event) => this.toggleSidenav() } >
                    <MenuItem style={styles.items} className="menu-item"
                        containerElement={<Link to={'/'} />}
                        primaryText="Image API"
                    />
                    <MenuItem style={styles.items} className="menu-item"
                        containerElement={<Link to={'/balance'} />}
                        primaryText="Balanced Brackets"
                    />
                    <MenuItem style={styles.items} className="menu-item"
                        containerElement={<Link to={'/task'} />}
                        primaryText="Date Formatter"
                    />
                    <MenuItem style={styles.items} className="menu-item"
                        containerElement={<Link to={'/note'} />}
                        primaryText="To Do List"
                    />
                </AppBar>
                <NavRes ref={this.child} />
            </div>
        ) 
    }
}
export default Navbar;
