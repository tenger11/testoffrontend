import React, { Component } from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router-dom';

class NavRes extends Component {

  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  handleToggle = () => this.setState({open: !this.state.open});

  handleClose = () => this.setState({open: false});

  render() {
    return (
      <div>
        <Drawer
          docked={false}
          width={200}
          open={this.state.open}
          onRequestChange={(open) => this.setState({open})}
        >
          <MenuItem
            containerElement={<Link to={'/'} />}
            primaryText="Image API" 
            onClick={this.handleClose}
          />
         <MenuItem
            containerElement={<Link to={'/balance'} />}
            primaryText="Balanced Brackets"
            onClick={this.handleClose}
          />
          <MenuItem
            containerElement={<Link to={'/task'} />}
            primaryText="Date Formatter"
            onClick={this.handleClose}
          />
          <MenuItem
            containerElement={<Link to={'/note'} />}
            primaryText="To Do List"
            onClick={this.handleClose}
          />
        </Drawer>
      </div>
    );
  }
}

export default NavRes;