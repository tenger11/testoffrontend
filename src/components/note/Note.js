import React, { Component } from 'react';
import Notes from './Notes';
import NoteAdd from './NoteAdd';
import fire from '../../config/fire';
import 'firebase/database';
class Note extends Component {
    constructor(props){
        super(props);

        this.state = {
            notes: []
        }

        this.addNote = this.addNote.bind(this);
        this.removeNote = this.removeNote.bind(this);
        this.updateNote = this.updateNote.bind(this);
    }

    componentWillMount(){
        const prevnotes = this.state.notes;
        this.db = fire.database().ref('notes');
        this.db.on('child_added', snap => {
            prevnotes.push({
                id: snap.key,
                content: snap.val().content,
                title: snap.val().title
            })

            this.setState({
                notes: prevnotes
            })
        })

        this.db.on('child_removed', snap => {
            prevnotes.forEach((note, i) => {
                if(note.id === snap.key)
                    prevnotes.splice(i, 1);
            })

            this.setState({
                notes: prevnotes
            })
        })

        this.db.on('child_changed', snap => {
            prevnotes.map((note, i) => {
                if(note.id === snap.key){
                    note.title = snap.val().title;
                    note.content = snap.val().content;
                }   
            })

            this.setState({
                notes: prevnotes
            })
        })
    }

    addNote(note, title){
        this.db.push().set({content: note, title: title})
    }
    
    removeNote(noteId){
        this.db.child(noteId).remove();
    }

    updateNote(noteId, data){
        this.db.child(noteId).update(data);
    }

    render() {
        return (
            <div className="container">
                    <NoteAdd addNote={this.addNote} />
                    {
                        this.state.notes.map((note) =>{
                        return(<Notes noteContent={note.content} noteTitle={note.title} key={note.id} noteId={note.id} removeNote={this.removeNote} updateNote={this.updateNote} />)
                        })
                    }
            </div>
        )
    }
}

export default Note;
