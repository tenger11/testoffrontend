import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
class NoteAdd extends Component {
    constructor(props){
        super(props);
        this.state = {
            noteContent: '',
            noteTitle: '',
            errorMessage: 'This field is required'
        }

        this.noteSubmit = this.noteSubmit.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);

    }

    noteSubmit(e){
        e.preventDefault();
        if(!this.state.noteTitle || !this.state.noteContent){
            return;
        }
        this.props.addNote(this.state.noteContent, this.state.noteTitle);

        this.setState({
            noteContent: '',
            noteTitle: ''
        })
    }

    onTextChange(e){
        this.setState({
            noteContent: e.target.value
        })
    }

    onTitleChange(e){
        this.setState({
            noteTitle: e.target.value
        })
    }

  render() {
    return (
        <form onSubmit={this.noteSubmit} className="form-add">
            <TextField
                name="note"
                value={this.state.noteTitle}
                onChange={this.onTitleChange}
                floatingLabelText="Write a title ..."
                fullWidth={false}
                errorText={(!this.state.noteTitle) ? this.state.errorMessage : ''}
            />
            <TextField
                name="note"
                value={this.state.noteContent}
                onChange={this.onTextChange}
                floatingLabelText="Write a Note ..."
                fullWidth={true}
                errorText={(!this.state.noteContent) ? this.state.errorMessage : ''}
            />
            <RaisedButton label="Add" secondary={true} type="submit" />
        </form>
    )
  }
}

export default NoteAdd;
