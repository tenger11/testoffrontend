import React, { Component } from 'react';
import {Card, CardActions, CardTitle , CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

class Notes extends Component {
    constructor(props){
        super(props);
        this.state = {
            open: false,
            noteContent: '',
            noteTitle: '',
            errorMessage: 'This field is required'
        }

        this.hanldeRemove = this.hanldeRemove.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
    }

    handleOpen = (id, noteTitle, noteContent) => {
        this.setState({open: true, currentID: id, noteContent: noteContent, noteTitle: noteTitle});
    }

    handleClose = () => {
        this.setState({open: false});
    }

    hanldeRemove = (id) => {
        const alert = window.confirm("Are you sure you wish to delete this item?");
        if(alert)
            this.props.removeNote(id);
    }

    handleUpdate = (e) => {
        if(!this.state.noteTitle || !this.state.noteContent){
            return;
        }
        const data = {content: this.state.noteContent, title: this.state.noteTitle}
        this.props.updateNote(this.props.noteId, data);

        this.setState({open: false});
    }

    onTextChange(e){
        this.setState({
            noteContent: e.target.value
        })
    }

    onChangeTitle(e){
        this.setState({
            noteTitle: e.target.value
        })
    }

    render() {
        const actions = [
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={this.handleClose}
            />,
            <FlatButton
              label="Modify"
              primary={true}
              onClick={this.handleUpdate}
            />,
          ];
        return (
            <div>
                <Card className="Card-space">
                    <CardTitle 
                    title={this.props.noteTitle}
                    actAsExpander={false}
                    showExpandableButton={false}
                    />
                    <CardText expandable={false}>
                        {this.props.noteContent}
                    </CardText>
                    <CardActions>
                        <RaisedButton primary={true} label="Edit" onClick={() => this.handleOpen(this.props.noteId, this.props.noteTitle, this.props.noteContent)} />
                        <RaisedButton label="Delete" onClick={() => this.hanldeRemove(this.props.noteId)} />
                    </CardActions>
                </Card>

                <Dialog 
                actions={actions}
                modal={false}
                open={this.state.open}
                onRequestClose={this.handleClose}
                >
                    <TextField
                        name="note"
                        value={this.state.noteTitle}
                        onChange={this.onChangeTitle}
                        floatingLabelText="Write a Title ..."
                        fullWidth={true}
                        errorText={(!this.state.noteTitle) ? this.state.errorMessage : ''}
                    />
                    <TextField
                        name="note"
                        value={this.state.noteContent}
                        onChange={this.onTextChange}
                        floatingLabelText="Write a Note ..."
                        fullWidth={true}
                        errorText={(!this.state.noteContent) ? this.state.errorMessage : ''}
                    />
                    
                </Dialog>
            </div>
        )
    }
}

export default Notes;
