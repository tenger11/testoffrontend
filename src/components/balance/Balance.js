import React, { Component } from 'react';
import TextField from 'material-ui/TextField';

class Balance extends Component {
    state = {
        searchText: '',
        balancedText: 'NO'
    }

    onTextChange = (e) => {
        const val = e.target.value;
        this.setState({
            [e.target.name] : val
        }, () =>{
            if(val === ''){
                this.setState({images: []})
            }else{
                const balance = this.getBalanced(val);
                this.setState({
                    balancedText : balance
                })
            }
        });
    }

    getBalanced = (input) => {
        let tokens = [ ['{','}'] , ['[',']'] , ['(',')'] ];
    
        let stack = [];
        let expression = input.split('');
        let result = true;
        expression.map((exp) => {
          if(this.isBrace(exp)){
            if(this.isOpenBrace(exp, tokens)){
              stack.push(exp);
            }else{
              if (stack.length === 0) { 
                result = false;
                return result;
              }
              let top = stack.pop();
              if (!this.matches(top, exp, tokens)) {
                result = false;
                return result;
              }
            }
          }else{
            result = false;
            return result;
          }
        });
        let returnValue = result && stack.length === 0 ? true : false;
        return this.resultBalance(returnValue);
    }
    
    isBrace = (char) => {
      let str = '{}[]()';
      if (str.indexOf(char) > -1) {
        return true;
      } else {
        return false;
      }
    }
    
    isOpenBrace = (brace, tokens) => {
      let open = false;
      tokens.map(token =>{
        if(token[0] === brace) {
          open = true;
          return;
        } 
      });
      return open;
    }
    
    matches = (topOfStack, closedBrace, tokens) => {
      let open = false;
      tokens.map(token => {
        if(token[0] === topOfStack && token[1] === closedBrace ) {
          open = true;
          return open;
        }
      });
      return open;
    }
    
    resultBalance = (bool) => {
      let isBalanced = ['YES', 'NO'];
      if (bool) {
        return isBalanced[0];
      } else {
        return isBalanced[1];
      }
    }
  render() {
    return (
      <div className="container">
        <TextField
            name="searchText"
            value={this.state.searchText}
            onChange={this.onTextChange}
            floatingLabelText="Type {}[]()... etc"
            fullWidth={true}
        />
        <br />
        <TextField
            name="Output"
            value={this.state.balancedText}
            floatingLabelText="OUTPUT"
        />
      </div>
    )
  }
}

export default Balance;
