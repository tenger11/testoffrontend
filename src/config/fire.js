import firebase from 'firebase'
const config = { /* COPY THE ACTUAL CONFIG FROM FIREBASE CONSOLE */
    apiKey: "AIzaSyCu4-ZejMBo_4HZRsPkZlYx2hA8d-0tw2c",
    authDomain: "reactapp-76673.firebaseapp.com",
    databaseURL: "https://reactapp-76673.firebaseio.com",
    projectId: "reactapp-76673",
    storageBucket: "reactapp-76673.appspot.com",
    messagingSenderId: "711700127978"
};
const fire = firebase.initializeApp(config);
export default fire;